package io.github.tubakyle.itemexchangesigns;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOError;

public final class ItemExchangeSigns extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }
    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        Player p = e.getPlayer();
        if (e.getLine(0).equalsIgnoreCase("[IES]")) {
            if (p.hasPermission("ies.create")) {
                if (e.getLine(1).isEmpty()) {
                    sendMessage(p, ChatColor.RED + "Line 2 is blank.");
                    resetSign(e);
                } else if (e.getLine(3).isEmpty()) {
                    sendMessage(p, ChatColor.RED + "Line 4 is blank.");
                    resetSign(e);
                } else if (Integer.parseInt(e.getLine(1).substring(0, 2)) < 0 | Integer.parseInt(e.getLine(1).substring(0, 2)) > 64) {
                    sendMessage(p, ChatColor.RED + "Item quantity in line 2 is invalid.");
                    resetSign(e);
                } else if (Integer.parseInt(e.getLine(3).substring(0, 2)) < 0 | Integer.parseInt(e.getLine(3).substring(0, 1)) > 64) {
                    sendMessage(p, ChatColor.RED + "Item quantity in line 3 is invalid.");
                    resetSign(e);
                } else {
                    sendMessage(p, ChatColor.GREEN + "Item Exchange Sign successfully created.");
                    String mat1 = e.getLine(1).substring(3).toUpperCase();
                    String mat2 = e.getLine(3).substring(3).toUpperCase();
                    String mat1q = e.getLine(1).substring(0, 2);
                    String mat2q = e.getLine(3).substring(0, 2);
                    try {
                        e.setLine(0, ChatColor.BLUE + "[IES]");
                        e.setLine(1, mat1q + " " + Material.valueOf(mat1).name().toLowerCase());
                        e.setLine(2, "to receive");
                        e.setLine(3, mat2q + " " + Material.valueOf(mat2).name().toLowerCase());
                    } catch (IOError io) {
                        resetSign(e);
                        sendMessage(p, ChatColor.RED + "Invalid material(s).");
                    }
                }
            } else {
                sendMessage(p, ChatColor.RED + "You don't have permission for that!");
                e.setLine(0, "");
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getState() instanceof Sign && p.hasPermission("ies.use")){
            Sign s = (Sign) e.getClickedBlock().getState();
            if (s.getLine(0).equalsIgnoreCase(ChatColor.BLUE + "[IES]")) {
                String a = s.getLine(1);
                String b = s.getLine(3);
                String itemFromPlayer = a.substring(3).toUpperCase();
                int QuanFromPlayer = Integer.parseInt(a.substring(0, 2));
                String itemToPlayer = b.substring(3).toUpperCase();
                int QuanToPlayer = Integer.parseInt(b.substring(0, 2));
                ItemStack itemsToRemove = new ItemStack(Material.valueOf(itemFromPlayer), QuanFromPlayer);
                ItemStack itemsToGive = new ItemStack(Material.valueOf(itemToPlayer), QuanToPlayer);
                Inventory i = p.getInventory();
                if (i.contains(Material.valueOf(itemFromPlayer), QuanFromPlayer)) {
                    i.removeItem(itemsToRemove);
                    i.addItem(itemsToGive);
                    p.updateInventory();
                    sendMessage(p, ChatColor.GREEN + "Items succesfully exchanged.");
                } else {
                    sendMessage(p, ChatColor.RED + "You do not have enough " + ChatColor.GOLD + itemsToRemove.getType().toString().substring(0, 1).toUpperCase() + itemsToRemove.getType().toString().substring(1).toLowerCase() + ChatColor.RED + ".");
                    sendMessage(p, ChatColor.RED + "You need " + QuanFromPlayer + " " + ChatColor.GOLD +  itemsToRemove.getType().toString().substring(0, 1).toUpperCase() + itemsToRemove.getType().toString().substring(1).toLowerCase() + ChatColor.RED + " to use this sign.");
                }
            }
        } else {
            return;
        }
    }
    public void sendMessage(Player p, String msg) {
        p.sendMessage(ChatColor.BLUE + "[IES] " + ChatColor.WHITE + msg);
    }
    public void resetSign(SignChangeEvent s) {
        s.setLine(0, "");
        s.setLine(1, "");
        s.setLine(2, "");
        s.setLine(3, "");
    }
}