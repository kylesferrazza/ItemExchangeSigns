# ItemExchangeSigns
This plugin allows users to create signs which can be used to trade-in items.

## How to use:
Create a sign.
The first line should be:
<pre>
[IES]
</pre>
The second line will be what the player has to trade in.

The third line will be auto-formatted to "to receive", showing the player that they will recieve the item(s) on the bottom line.

The fourth line will be what the player will receive.

The second and fourth lines are formatted as follows:
<pre>
[QUANTITY] [MATERIAL]
</pre>
Quantity is an integer from 01 to 64.

Make sure there is a space between the quantity and the material.

Material is a material name. Material names can be found <a href = "http://jd.bukkit.org/rb/apidocs/org/bukkit/Material.html">here.</a>

For example, a sign that reads
<pre>
[IES]
05 PAPER
(Leave this line blank)
01 DIAMOND
</pre>
will take 5 paper from the player in exchange for 1 diamond.

## Permissions:

<pre>
ies.*:
  description: Allows a player to use all ItemExchangeSigns commands.
ies.use:
  description: Allows a player to use an Item Exchange Sign.
  Defaults to everyone.
ies.create:
  description: Allows a player to create an Item Exchange Sign.
  Defaults to server op's.
</pre>
